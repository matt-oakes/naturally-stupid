package naturallystupid;

import java.util.Observable;

/**
 * Representation of the Kalah board.<BR><BR>
 * The board has two sides: "North" and
 * "South". On each side there is a number of linearly arranged "holes" (the
 * same number on each side) and a "store" for each side. Holes are numbered
 * per side, starting with 1 on "the left" (i.e. furthest away from the
 * player's store, the numbers increase in playing direction).
 * <BR>
 * Initially, there is the same number of "seeds" in each hole.
 */
public class Board extends Observable implements Cloneable
{
	private static final int STORE_INDEX = 0;

	/**
	 * The number of holes per side (must be >= 1).
	 */
	private final int holes;

	/**
	 * The board data. The first dimension of the array is 2, the second one
	 * is the number of holes per side plus one. The data for the North side
	 * is stored in board[NORTH_ROW][*], the data for the South side in
	 * board[SOUTH_ROW][*]. The number of seeds in hole number n (of one side)
	 * is stored in board[...][n], the number of seeds in the store (of one
	 * side) is stored in board[...][0].
	 */
	private int[] board;

    private int pitIndex(Side side, int pit) {
        if (side == Side.NORTH) {
            pit += holes + 1;
        }

        return pit;
    }


    /**
     * Creates a new board.
     *
     * @param holes The number of holes per side (must be >= 1).
     * @param seeds The initial number of seeds per hole (must be >= 0). The
     *        stores are empty initially.
     * @throws IllegalArgumentException if any of the arguments is outside of
     *         the valid range.
     */
    public Board (int holes, int seeds) throws IllegalArgumentException {
    	if (holes < 1)
    		throw new IllegalArgumentException("There has to be at least one hole, but " + holes + " were requested.");
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	this.holes = holes;
        int numberOfParts = (holes + 1) * 2;
    	board = new int[numberOfParts]; // WARNING: potential integer overflow here!

    	for (int i = 1; i <= holes; i++) {
    		board[pitIndex(Side.SOUTH, i)] = seeds;
    		board[pitIndex(Side.NORTH, i)] = seeds;
    	}
    }

	/**
     * Creates a new board as the copy of a given one. Both copies can then be
     * altered independently.
     *
     * @param original The board to copy.
     * @see #clone()
     */
    public Board (Board original) {
    	holes = original.holes;
    	board = original.board.clone();
    }

    /**
     * Creates a new board with the number of holes and the seeds
     * WARNING: NO NOT EDIT A BOARD THAT YOU CREATE WITH THIS METHOD
     */
    public Board (int holes, int[] givenBoard) {
        this.holes = holes;
        this.board = givenBoard.clone();
    }

    /**
     * Creates a copy of the current board. Both copies can then be altered
     * independently.
     *
     * @see java.lang.Object#clone()
     * @see #Board(Board)
     */
    @Override
	public Board clone() {
    	return new Board(this);
	}

    /**
     * @return The number of holes per side (will be >= 1).
     */
    public int getNoOfHoles () {
		return holes;
    }

    public int[] getBoardState() {
        return board;
    }

    /**
     * Get the number of seeds in a hole.
     * @param side The side the hole is located on.
     * @param hole The number of the hole.
     * @return The number of seeds in hole "hole" on side "side".
     * @throws IllegalArgumentException if the hole number is invalid.
     */
    public int getSeeds (Side side, int hole) throws IllegalArgumentException {
    	if (hole < 1 || hole > holes)
    		throw new IllegalArgumentException("Hole number must be between 1 and " + holes + " but was " + hole + ".");

    	return board[pitIndex(side, hole)];
    }

    /**
     * Sets the number of seeds in a hole.
     * @param side The side the hole is located on.
     * @param hole The number of the hole.
     * @param seeds The number of seeds that shall be in the hole afterwards (>= 0).
     * @throws IllegalArgumentException if any of the arguments is outside of
     *         the valid range.
     */
    public void setSeeds (Side side, int hole, int seeds) throws IllegalArgumentException {
    	if (hole < 1 || hole > holes)
    		throw new IllegalArgumentException("Hole number must be between 1 and " + holes + " but was " + hole + ".");
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	board[pitIndex(side, hole)] = seeds;
    	this.setChanged();
    }

    /**
     * Adds seeds to a hole.
     * @param side The side the hole is located on.
     * @param hole The number of the hole.
     * @param seeds The number (>= 0) of seeds to put into (add to) the hole.
     * @throws IllegalArgumentException if any of the arguments is outside of
     *         the valid range.
     */
    public void addSeeds (Side side, int hole, int seeds) throws IllegalArgumentException {
    	if (hole < 1 || hole > holes)
    		throw new IllegalArgumentException("Hole number must be between 1 and " + holes + " but was " + hole + ".");
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	board[pitIndex(side, hole)] += seeds;
    	this.setChanged();
    }

    /**
     * Get the number of seeds in a hole opposite to a given one.
     * @param side The side the given hole is located on.
     * @param hole The number of the given hole.
     * @return The number of seeds in the hole opposite to hole "hole" on
     *         side "side".
     * @throws IllegalArgumentException if the hole number is invalid.
     */
    public int getSeedsOp (Side side, int hole) throws IllegalArgumentException
    {
    	if (hole < 1 || hole > holes)
    		throw new IllegalArgumentException("Hole number must be between 1 and " + holes + " but was " + hole + ".");

    	return board[pitIndex(side.opposite(), holes+1-hole)];
    }

    /**
     * Sets the number of seeds in a hole opposite to a given one.
     * @param side The side the given hole is located on.
     * @param hole The number of the given hole.
     * @param seeds The number of seeds that shall be in the hole opposite to
     *        hole "hole" on side "side" afterwards (>= 0).
     * @throws IllegalArgumentException if any of the arguments is outside of
     *         the valid range.
     */
    public void setSeedsOp (Side side, int hole, int seeds) throws IllegalArgumentException {
    	if (hole < 1 || hole > holes)
    		throw new IllegalArgumentException("Hole number must be between 1 and " + holes + " but was " + hole + ".");
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	board[pitIndex(side.opposite(), holes+1-hole)] = seeds;
    	this.setChanged();
    }

    /**
     * Adds seeds to a hole opposite to a given one.
     * @param side The side the given hole is located on.
     * @param hole The number of the given hole.
     * @param seeds The number (>= 0) of seeds to put into (add to) the hole opposite to
     *        hole "hole" on side "side" afterwards (>= 0).
     * @throws IllegalArgumentException if any of the arguments is outside of
     *         the valid range.
     */
    public void addSeedsOp (Side side, int hole, int seeds) throws IllegalArgumentException
    {
    	if (hole < 1 || hole > holes)
    		throw new IllegalArgumentException("Hole number must be between 1 and " + holes + " but was " + hole + ".");
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	board[pitIndex(side.opposite(), holes+1-hole)] += seeds;
    	this.setChanged();
    }

    /**
     * Get the number of seeds in a store.
     * @param side The side the store is located on.
     * @return The number of seeds in the store.
     */
    public int getSeedsInStore(Side side)
    {
		return board[pitIndex(side, STORE_INDEX)];
    }

    /**
     * Sets the number of seeds in a store.
     * @param side The side the store is located on.
     * @param seeds The number of seeds that shall be in the store afterwards (>= 0).
     * @throws IllegalArgumentException if the number of seeds is invalid.
     */
    public void setSeedsInStore(Side side, int seeds) throws IllegalArgumentException
    {
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	board[pitIndex(side, STORE_INDEX)] = seeds;
    	this.setChanged();
    }

    /**
     * Adds seeds to a store.
     * @param side The side the store is located on.
     * @param seeds The number (>= 0) of seeds to put into (add to) the store.
     * @throws IllegalArgumentException if the number of seeds is invalid.
     */
    public void addSeedsToStore(Side side, int seeds) throws IllegalArgumentException
    {
    	if (seeds < 0)
    		throw new IllegalArgumentException("There has to be a non-negative number of seeds, but " + seeds + " were requested.");

    	board[pitIndex(side, STORE_INDEX)] += seeds;
    	this.setChanged();
    }

	@Override
	public String toString()
	{
		StringBuilder boardString = new StringBuilder();

		boardString.append(getSeedsInStore(Side.NORTH) + "  --");
		for (int i=holes; i >= 1; i--)
			boardString.append("  " + getSeeds(Side.NORTH, i));
		boardString.append("\n");
		for (int i=1; i <= holes; i++)
			boardString.append(getSeeds(Side.SOUTH, i) + "  ");
		boardString.append("--  " + getSeedsInStore(Side.SOUTH) + "\n");

		return boardString.toString();
	}
}

