package naturallystupid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Reader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.Iterator;
import java.util.List;

/**
 * The main application class. It also provides methods for communication
 * with the game engine.
 */

 /*
  * TODO: Implement tree structure
 */

public class Main {
    /**
     * The style files which we will log to
     **/
    private static final String LOG_FILE_1 = "log1.txt";
    private static final String ERROR_FILE_1 = "error1.txt";
    private static int MAX_SEARCH_DEPTH = 11;
    private static final boolean DEBUG = false;

    /**
     * The move number we are current on
     **/
    private static int moveNo = 0;
    /**
     * Input from the game engine.
     */
    private static Reader input = new BufferedReader(new InputStreamReader(System.in));
    /**
     * A flag to show if we've sent the move
     * This is used when we get a state message to tell if it's our move we're getting
     **/
    private static boolean moveSent = false;
    /**
     * The current game board
     **/
    private static Board board;
    /**
     * The kalah game object
     **/
    private static Kalah kalah;
    /**
     * The side which we are playing on
     **/
    private static Side ourSide;
    /**
     * Executes the tree task on its threads
     **/
    private static ExecutorService treeExecutor = Executors.newFixedThreadPool(10);
    /**
     * Enabled experimental scoring of seeds on our side
     **/
    private static boolean experimentalScoringEnabled = false;

    /**
     * The main method, invoked when the program is started.
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        if (args.length >= 1) {
            try {
                MAX_SEARCH_DEPTH = new Integer(args[0]);
            }
            catch (NumberFormatException e) {}
        }
        if (args.length >= 2) {
            experimentalScoringEnabled = true;
        }

        // Create the board
        board = new Board(7, 7);
        // Create the kalah game with the 7 7 board
        kalah = new Kalah(board);
        // This will store our side once we are told by the start state
        ourSide = Side.SOUTH;

        while (!kalah.gameOver(board)) {
            try {
                // Get the next message from the game server
                String msg = recvMsg();
                // Get the message type
                MsgType type = Protocol.getMessageType(msg);
                switch (type) {
                // This is called at the start of the game
                // It will tell you which side you are
                case START:
                    startState(msg);
                    break;
                // This is called when a move has been made, including our own moves
                case STATE:
                    moveState(msg);
                    break;
                // This is called when the game ends
                case END:
                    endState(msg);
                    break;
                }
            }
            catch (Exception e) {
                logMsg("Exception: " + e);
            }
        }
    }

    /**
     * Deal with the start message
     **/
    private static void startState(String msg) throws InvalidMessageException {
        logMsg("PROT:Start");

        // Interpret the start message
        boolean isSouth = Protocol.interpretStartMsg(msg);
        // Set our side
        ourSide = (isSouth) ? Side.SOUTH : Side.NORTH;

        // If we're south then we move first
        if (isSouth) {
            sendMove(1);
        }
    }

    /**
     * Deal with the state message
     **/
    private static void moveState(String msg) throws InvalidMessageException {
        logMsg("PROT:State");

        // Interpret the message and get the turn
        Protocol.MoveTurn turn = Protocol.interpretStateMsg(msg, board);

        // Check if we've received a swap message
        if (turn.move == -1) {
            // Swap our side
            ourSide = ourSide.opposite();
        }
        // If not we update out board
        else {
            // Work out which side this state is for by checking if we last sent a move
            Side side = ourSide.opposite();
            if (moveSent) {
                side = ourSide;
                moveSent = false;
            }

            // Make the move on the board using the kalah game
            kalah.makeMove(new Move(side, turn.move));
        }

        logMsg("PROT:Board State\n" + kalah.getBoard());

        // If it's our turn then make a move
        if (turn.ourMove) {
            makeMove();
        }
    }

    /**
     * Deal with the end game message
     **/
    private static void endState(String msg) throws InvalidMessageException {
        logMsg("PROT:End");
    }

    private static void makeMove() {
        makeTreeMove();
    }

    /**
     * Sends the move to the server and updates the internal state
     * @param pit The pit number to move (1-7)
     **/
    private static void sendMove(int pit) {
        // Log out the board state
        logMsg("MOVE:" + board);
        logMsg("MOVE:Pit: " + pit);
        logMsg("Move Number: " + moveNo);
        logMsg("MOVE:###");

        // Create the protocol move message
        String moveMsg = Protocol.createMoveMsg(pit);
        // Send the move over std out
        sendMsg(moveMsg);
        // Set the move sent flag so we know it's our state when the next state message is received
        moveSent = true;
        // Increment the move number
        moveNo++;
    }

    /**
     * Makes a random move, making sure that it is valid
     **/
    private static void makeRandomMove() {
        // Chooses a random pit, making sure it is a valid move
        int random;
        Move move;
        do {
            random = getRandomPit();
            move = new Move(ourSide, random);
        } while(!kalah.isLegalMove(move));

        // Make da move!
        sendMove(random);
    }

    /**
     * Swaps our board side and sends the swap message to the game server
     **/
    private static void makeSwapMove() {
        ourSide = ourSide.opposite();
        String swapMsg = Protocol.createSwapMsg();
        sendMsg(swapMsg);
        moveNo++;
    }

    /**
     * Returns a random pit number (1-7)
     * @return a random int between (1-7)
     **/
    private static int getRandomPit() {
        return (int) (Math.random() * 7) + 1;
    }

    /**
     * Makes a move with heurstics(ish)
     * Will also do tree searching
     */
    private static void makeHeuristicsMove() {
      try {
       // ----------add heuristics first---------//
           // ------ free Move  -------//
    	   int pit  = freeMove(board);
           if (pit > 0) {
                logMsg("MOVE:Free");
                sendMove(pit);
                return;
            }
           // ------ capture -----------//
           pit = capture(board);
           if (pit > 0) {
                logMsg("MOVE:Capture");
                sendMove(pit);
                return;
            }
            // ------- anti capture ------- //
            pit = antiCapture(board);
            if (pit > 0) {
                logMsg("MOVE:Anti Capture");
                sendMove(pit);
                return;
            }
            // -------- anti free move ------ //
            pit = antiFreeMove(board);
            if (pit > 0) {
                logMsg("MOVE:Anti Free");
                sendMove(pit);
                return;
            }

            logMsg("MOVE:Random");
           makeRandomMove();
	   //------then add the tree searching and pruning ----//
      }
      catch (Exception e) {
        logMsg(e.toString());
      }
    }

    /**
     * Finds a pit that contains the required amount of seeds for a anti free move
     * @return index of the pit to make the move, else -1 if no anti free move
     */
    private static int antiFreeMove(Board board) {
        // NOTES: check the move wont give a capture to the other team

        // reqSeeds is the number of seeds required in the pit for a free move
        int reqSeeds = 1;
        // reqSeedsUs is the number of seeds required to stop the free move
        int reqSeedsUs = 1;
        // contains the pit index of the pit that holds the free move
        int pit = 0;
        // for each pit check if it contains the required number of the seeds
        // for a free move on the opposite board
        for (int i = 7; i > 0; i--) {
            if (board.getSeeds(ourSide.opposite(), i) == reqSeeds) {
                pit = i;
                break;
            }
            reqSeeds++;
        }

        // find a pit with enough seeds to stop the free go, and return its number
        for (int i = 7; i > 0; i--) {
            if (board.getSeeds(ourSide, i) >= ((7 - reqSeeds) + 1 + reqSeedsUs)) {
                return i;
            }
            reqSeedsUs++;
        }

        // no anti free move
        return -1;
    }

    /**
     * Finds a pit that contains the required amount of seeds for an anti capture
     * @return index of the pit to make the move, else -1 if no anti capture
     */
    private static int antiCapture(Board board) {
        //NOTES : check that the anti capture doesnt give them a better capture

        int reqSeeds = 1;
        int seeds;
        int capAmount = 0;
        int bestPit = -1;
        for (int i = 7; i > 0; i--) {
            seeds = board.getSeeds(ourSide.opposite(),i);
            if (seeds >= reqSeeds || seeds < reqSeeds + 8 || seeds == 0)
                break;
            if (seeds < reqSeeds) {
                if ((capAmount = board.getSeedsOp(ourSide.opposite(), 8 - (reqSeeds - seeds))) > capAmount ) {
                    capAmount = board.getSeedsOp(ourSide.opposite(), 8 - (reqSeeds - seeds));
                    bestPit = i;
                }
            }
            else {
                if ((capAmount = board.getSeedsOp(ourSide.opposite(), seeds - 8 - (reqSeeds-1))) > capAmount) {
                    capAmount = board.getSeedsOp(ourSide.opposite(), seeds - 8 - (reqSeeds-1));
                    bestPit = i;
                }
            }
            reqSeeds++;
        }
        return bestPit;
    }

    /**
     * Finds a pit that contains the required amount of seeds for a capture
     * @return index of the pit to make the move, else -1 if no capture
     */
    private static int capture(Board board) {
        int reqSeeds = 1;
        int seeds;
        int capAmount = 0;
        int bestPit = -1;
        for (int i = 7; i > 0; i--) {
            seeds  = board.getSeeds(ourSide, i);
            if (seeds >= reqSeeds || seeds < reqSeeds+8  || seeds == 0)
                break;
            if (seeds < reqSeeds) {
                if ((capAmount = board.getSeedsOp(ourSide, 8 - (reqSeeds - seeds))) > capAmount ) {
                    capAmount = board.getSeedsOp(ourSide, 8 - (reqSeeds - seeds));
                    bestPit = i;
                }
            }
            else {
                if((capAmount = board.getSeedsOp(ourSide, seeds - 8 - (reqSeeds-1))) > capAmount) {
                    capAmount = board.getSeedsOp(ourSide, seeds - 8 - (reqSeeds-1));
                    bestPit = i;
                }
            }
            reqSeeds++;
        }
        return bestPit;
    }

    /**
     * Finds a pit that contains the required amount of seeds for a free move
     * @return index of the pit to make the move, else -1 if no free move
     */
    private static int freeMove(Board board) {
        // reqSeeds is the numberof seeds required in the pit for a free move
        int reqSeeds = 1;
        // for each pit check if it contains the required number of the seeds
        for (int i = 7; i > 0; i--) {
            if (board.getSeeds(ourSide, i) == reqSeeds) {
                return i;
            }
            reqSeeds++;
        }
        return -1;
    }

    private static void makeTreeMove() {
        ArrayList<TreeTask> callables = new ArrayList<TreeTask>(7);

        for (int i = 1; i <= 7; i++) {
            Move move = new Move(ourSide, i);
            if (kalah.isLegalMove(board, move)) {
                Board newBoard = new Board(board);
                Side nextSide = kalah.makeMove(newBoard, move);
                callables.add(new TreeTask(i, newBoard.getBoardState(), nextSide));
            }
        }

        int bestScore = Integer.MIN_VALUE;
        int bestMove = Integer.MIN_VALUE;

        try {
            List<Future<TreeResult>> results = treeExecutor.invokeAll(callables);
            logMsg("Threads returned!");


            Iterator<Future<TreeResult>> iterator = results.iterator();
            while (iterator.hasNext()) {
                TreeResult result = iterator.next().get();
                if (result.score >= bestScore) {
                    bestScore = result.score;
                    bestMove = result.movePit;
                    logMsg("New best overall score at (" + bestMove + ") with score " + bestScore);
                }
            }
        }
        catch (Exception e) {
            logMsg("FAIL! Threads caused exception. Making random move as fail safe");
            makeRandomMove();
            return;
        }

        // This should never happen, but I've left it in to make sure we always make a move
        if (bestMove == Integer.MIN_VALUE) {
            logMsg("FAIL! Didn't suggest a best move, making a random one instead");
            makeRandomMove();
            return;
        }

        logMsg("Overall score at (" + bestMove + ") with score " + bestScore);
        sendMove(bestMove);
    }

    private static class TreeTask implements Callable<TreeResult> {
        private int[] boardState;
        private TreeResult result = new TreeResult();
        private Side nextSide;

        public TreeTask(int givenMovePit, int[] givenBoardState, Side givenNextSide) {
            result.movePit = givenMovePit;
            boardState = givenBoardState;
            nextSide = givenNextSide;
        }

        @Override
        public TreeResult call() {
            result.score = makeGameTree(boardState, nextSide, MAX_SEARCH_DEPTH, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return result;
        }
    }
    private static class TreeResult {
        public int movePit;
        public int score;
    }

    private static int makeGameTree(int[] boardState, Side moveSide, int depth, int alpha, int beta) {
        Board board = new Board(7, boardState);
        boolean isMax = moveSide.equals(ourSide);

        // logMsg(tabs + "Board State\n" + gameBoard);
        // logMsg(tabs + "Max player? " + isMax);

        if (kalah.gameOver(board) || depth == 0) {
            // logMsg(tabs + "End of tree at depth " + state.depth);
            int moveScore = getStateScore(board);
            // logMsg(tabs + "Score " + moveScore);
            return moveScore;
        }

        for (int i = 7; i >= 1; i--) {
            Move move = new Move(moveSide, i);
            if (kalah.isLegalMove(board, move)) {
                Board nextBoard = new Board(board);
                Side nextSide = kalah.makeMove(nextBoard, move);
                // makeMove(nextBoard, moveSide, move);

                // logMsg(tabs + "Making tree at depth " + state.depth + " for pit " + i);
                int moveScore = makeGameTree(nextBoard.getBoardState(), nextSide, depth - 1, alpha, beta);

                // Max player
                if (isMax) {
                    // Update our alpha value
                    alpha = Math.max(alpha, moveScore);
                }
                // Min player
                else {
                    // Update out beta value
                    beta = Math.min(beta, moveScore);
                }

                // If alpha is bigger than beta then break the loop
                if (beta <= alpha) {
                    break;
                }
            }
        }

        // Max player
        return (isMax) ? alpha : beta;
    }



    private static int getStateScore(Board board) {
        boolean gameOver = kalah.gameOver(board);
        int storeOurs = board.getSeedsInStore(ourSide);
        int storeTheirs = board.getSeedsInStore(ourSide.opposite());

        int score = storeOurs - storeTheirs;

        if (gameOver && score > 0) {
            return Integer.MAX_VALUE;
        }
        else if (gameOver && score < 0) {
            return Integer.MIN_VALUE;
        }

        // This seems to make no difference and just slow us down a bit, more testing resuired?
        // int totalOurs = 0;
        // int totalTheirs = 0;
        // for (int i = 1; i<= 7; i++) {
        //     totalOurs += board.getSeeds(ourSide, i);
        //     totalTheirs += board.getSeeds(ourSide.opposite(), i);
        // }

        // if ((totalOurs + totalTheirs) < (storeOurs + storeTheirs)) {
        //     return score + ((int) (totalOurs * 0.1));
        // }

        // ------ free Move  -------//
        if (freeMove(board) > 0) {
            return score + 4;
        }
        // ------ capture -----------//
        else if (capture(board) > 0) {
            return score + 3;
        }
        // ------- anti capture ------- //
        else if (antiCapture(board) > 0) {
            return score + 2;
        }
        // -------- anti free move ------ //
        else if (antiFreeMove(board) > 0) {
            return score + 1;
        }

        return score;
    }

    /**
     * Logs out a message to the correct log file
     * @param message The message to log out
     **/
    private static void logMsg(String message) {
        if (DEBUG) {
            try{
                BufferedWriter logOut;
                logOut = new BufferedWriter(new FileWriter(LOG_FILE_1, true));
                logOut.write(message + "\n");
                logOut.close();
            }
            catch (Exception e){
                System.err.println("Error: " + e.getMessage());
            }
        }
    }

    /**
     * Sends a message to the game engine.
     * @param msg The message.
     */
    private static void sendMsg(String msg) {
        System.out.print(msg);
        System.out.flush();
    }

    /**
     * Receives a message from the game engine. Messages are terminated by a '\n' character
     * @return The message.
     * @throws IOException if there has been an I/O error.
     */
    private static String recvMsg() throws IOException {
        StringBuilder message = new StringBuilder();
        int newCharacter;

        do {
            newCharacter = input.read();
            if (newCharacter == -1) {
                throw new EOFException("Input ended unexpectedly.");
            }
            message.append((char)newCharacter);
        } while((char)newCharacter != '\n');

        return message.toString();
    }

    public void redirectSystemErr() {

        try {
            FileOutputStream errOut;
            errOut = new FileOutputStream(ERROR_FILE_1);
            System.setErr(new PrintStream(errOut));
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
